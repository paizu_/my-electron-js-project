import Vue from 'vue'
import VueRouter from 'vue-router'
import Pass from '../views/Pass.vue'
import ChangeSecureKpp from "@/views/ChangeSecureKpp";
import InOutDrive from "@/views/InOutDrive";
import InOutPhysical from "@/views/InOutPhysical";
import Statistics from "@/views/Statistics";

Vue.use(VueRouter)

const routes = [
  {
    path: '/pass',
    name: 'Pass',
    component: Pass
  },
  {
    path: '/change-secure-kpp',
    name: 'ChangeSecureKpp',
    component: ChangeSecureKpp
  },
  {
    path: '/in-out-physical',
    name: 'InOutPhysical',
    component: InOutPhysical
  },
  {
    path: '/in-out-drive',
    name: 'InOutDrive',
    component: InOutDrive
  },
  {
    path: '/statistics',
    name: 'Statistics',
    component: Statistics
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
